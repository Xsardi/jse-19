package ru.t1.tbobkov.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IUserService getUserService();

    IAuthService getAuthService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

}
