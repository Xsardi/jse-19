package ru.t1.tbobkov.tm.api.repository;

import ru.t1.tbobkov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void clear();

    List<M> findAll();

    List<M> findAll(final Comparator<M> comparator);

    M add(final M model);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    int getSize();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

}
