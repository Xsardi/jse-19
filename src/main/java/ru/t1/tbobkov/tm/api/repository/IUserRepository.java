package ru.t1.tbobkov.tm.api.repository;

import ru.t1.tbobkov.tm.enumerated.Role;
import ru.t1.tbobkov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String login);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
