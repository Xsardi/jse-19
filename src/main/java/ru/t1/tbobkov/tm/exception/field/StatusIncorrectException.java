package ru.t1.tbobkov.tm.exception.field;

public class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error! Status is incorrect...");
    }

}
