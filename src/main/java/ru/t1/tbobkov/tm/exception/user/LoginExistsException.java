package ru.t1.tbobkov.tm.exception.user;

public final class LoginExistsException extends AbstractUserException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
