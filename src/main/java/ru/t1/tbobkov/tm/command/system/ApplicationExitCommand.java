package ru.t1.tbobkov.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String DESCRIPTION = "close application";

    public static final String NAME = "exit";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
