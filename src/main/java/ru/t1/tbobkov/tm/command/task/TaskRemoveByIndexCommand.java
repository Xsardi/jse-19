package ru.t1.tbobkov.tm.command.task;

import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    private static final String NAME = "task-remove-by-index";

    private static final String DESCRIPTION = "find task by index and remove it from storage";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(index);
    }

}
